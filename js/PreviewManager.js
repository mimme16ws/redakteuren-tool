var PreviewManager = PreviewManager || {};
PreviewManager = (function () {

    var that = {};

    function displayPreviewInElement(element) {
        var draft = EditorViewController.getDraft();
        sessionStorage.previewDraftID = draft.id;

        ServerManager.saveDraft(draft)
        .then(function() {
            element.innerHTML =
                "<i class='material-icons' id='preview-close' onclick='PreviewManager.hidePreview();'' style='margin: auto;''>cancel</i>" +
                "<div id='preview' style='margin: auto;''></div>";

            bioMp(document.getElementById("preview"), {
                url: "preview-content.html",
                view: "front",
                image: "res/assets/iphone6_front_black.png"
            });
        });
    }

    function hidePreview() {
        EditorViewController.fillEditorWithDraft(Draft.deserializedFrom(sessionStorage.previewDraft));
    }

    that.displayPreviewInElement = displayPreviewInElement;
    that.hidePreview = hidePreview;
    return that;
}());
