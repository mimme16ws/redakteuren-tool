var OnboardingController = OnboardingController || {};
OnboardingController = (function () {
    "use strict";
    /* eslint-env browser  */

    var that = {};

    function init() {
        ServerManager.init();
        addEventListeners();
    }

    function addEventListeners() {
        addLoginButtonEventListener();
        addNewArticleButtonListener();
        addReturnButtonListener();
    }

    function addReturnButtonListener() {
        window.onkeyup = function(e) {
            var key = e.keyCode ? e.keyCode : e.which;
            if (key == 13) {
                performLogin();
            }
        };
    }

    function addLoginButtonEventListener() {
        var loginButton = document.getElementById("submit-button");
        loginButton.addEventListener("click", function() {
            performLogin();
        });
    }

    function addNewArticleButtonListener() {
        document.getElementById("new-article-button").addEventListener("click", function() {
            AppService.deleteCachedDraft();
            window.location = "index.html";
        });
    }

    function performLogin() {
        var usernameInput = $("#username-input")[0].value;
        var passwordInput = $("#password-input")[0].value;
        hideLoginErrorLabel();
        AppService.showLoadingSpinnerWithId("login");
        AppService.login(usernameInput, passwordInput)
        .then(function(user) {
            sessionStorage.userCredentials = JSON.stringify({
                username: usernameInput,
                password: passwordInput
            });
            configureWelcomeLabelWithUser(user);
            return ServerManager.fetchLatestDraft(user);
        })
        .then(function(draft) {
            AppService.hideLoadingSpinnerWithId("login");
            if (draft) {
                showDraftSelection();
            } else {
                AppService.showMainPage();
            }
            return;
        },
        function(error) {
            processError(error);
        })
        .always(function() {
            AppService.hideLoadingSpinnerWithId("login");
        });

        function processError(error) {
            var errorMessage;
            var statusCode = error.code;
            switch (statusCode) {
            case 101:
                errorMessage = "Wrong username or password.";
                break;
            case 404:
                errorMessage = "Wrong username or password.";
                break;
            default:
                errorMessage = error.message;
                break;
            }
            showLoginErrorLabelWithMessage(errorMessage);
        }
    }

    function showLoginErrorLabelWithMessage(message) {
        var errorLabel = document.getElementById("wrong-password-label");
        errorLabel.innerText = message;
        errorLabel.style.display = "inline-block";
    }

    function hideLoginErrorLabel() {
        document.getElementById("wrong-password-label").style.display = "none;";
    }

    function configureWelcomeLabelWithUser(user) {
        var welcomeLabel = document.getElementById("welcome-label");
        welcomeLabel.innerText = "Welcome back " + user.get("username") + "!";
    }

    function showDraftSelection() {
        var draftSelectionBlock = document.getElementById("draft-selection-block");
        var continueDraftHeading = document.getElementById("continue-draft-heading");
        var draftGrid = document.getElementsByClassName("article-list")[0];
        var loginBlock = document.getElementById("login-block");
        ArticleList.init();
        ArticleList.addEventListener("didSelectItem", didSelectDraft);
        draftSelectionBlock.style.display = "block";
        continueDraftHeading.style.display = "block";
        draftGrid.style.display = "block";
        loginBlock.style.display = "none";
    }

    function didSelectDraft(event) {
        sessionStorage.selectedDraft = event.data.serialized();
        AppService.showMainPage();
    }

    that.init = init;
    return that;
}());
