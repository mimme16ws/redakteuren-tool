var EditorViewController = EditorViewController || {};
EditorViewController = (function () {
    "use strict";
    /* eslint-env browser  */
    /* global EventPublisher */

    var that = {},
        article = new Draft(),
        editor = document.getElementById("text-editor"),
        latestFormatDate = new Date();

    function init() {
        EditorView.init();
        addEventListeners();
    }

    function initWithDraft(draft) {
        init();
        article = draft ? draft : new Draft();
        fillEditorWithDraft(article);
    }

    function fillEditorWithDraft(draft) {
        var titleElement = document.getElementById("title-element");
        var pictureArea = document.getElementById("picture-area");
        editor.innerHTML = draft.html;
        titleElement.innerText = draft.title.replace(/\\n/g, " ");
        if (draft.imageURL) {
            pictureArea.style.background = "url(" + draft.imageURL + ") no-repeat center center";
        } else {
            pictureArea.style.background = "";
        }
        pictureArea.style.backgroundSize = "cover";
    }

    function addEventListeners() {
        EditorView.addEventListener("titleDidChange", onTitleChanged);
        EditorView.addEventListener("textDidChange", onTextHTMLChanged);
        EditorView.addEventListener("titleImageURLDidChange", onTitleImageURLChanged);
        SideMenu.addEventListener("boldButtonClicked", onBoldButtonClicked);
        SideMenu.addEventListener("italicButtonClicked", onItalicButtonClicked);
        SideMenu.addEventListener("underlineButtonClicked", onUnderlineButtonClicked);
        SideMenu.addEventListener("strikethroughButtonClicked", onStrikeThroughButtonClicked);
        SideMenu.addEventListener("alignCenterButtonClicked", onAlignCenterButtonClicked);
        SideMenu.addEventListener("alignJustifyButtonClicked", onAlignJustifyButtonClicked);
        SideMenu.addEventListener("alignLeftButtonClicked", onAlignLeftButtonClicked);
        SideMenu.addEventListener("alignRightButtonClicked", onAlignRightButtonClicked);
        SideMenu.addEventListener("indentIncreaseButtonClicked", onIndentIncreaseButtonClicked);
        SideMenu.addEventListener("indentDecreaseButtonClicked", onIndentDecreaseButtonClicked);
        SideMenu.addEventListener("textLevelHeadingButtonClicked", onTextLevelHeadingButtonClicked);
        SideMenu.addEventListener("textLevelTextButtonClicked", onTextLevelTextButtonClicked);
        SideMenu.addEventListener("textLevelQuoteButtonClicked", onTextLevelQuoteButtonClicked);
        SideMenu.addEventListener("saveButtonClicked", onSaveButtonClicked);
        SideMenu.addEventListener("previewButtonClicked", onPreviewButtonClicked);
        SideMenu.addEventListener("deleteButtonClicked", onDeleteButtonClicked);
        ArticleList.addEventListener("didSelectItem", onDraftSelected);
    }

    function onTitleImageURLChanged(event) {
        var imageFile = new Parse.File("titleImage.png", { base64: event.data });
        AppService.showLoadingSpinnerWithId("title");
        article.image = imageFile;
        document.getElementById("picture-area").style.opacity = "0.4";
        ServerManager.saveDraft(article)
        .then(function(savedDraft) {
            article.imageURL = savedDraft.get("image")._url;
            return ServerManager.saveDraft(article);
        })
        .then(function() {
            AppService.hideLoadingSpinnerWithId("title");
        },
        function(error) {
            if (error.code === 413) {
                alert("Your title image is too large. Please scale it down or use another image.");
            }
        })
        .always(function() {
            document.getElementById("picture-area").style.opacity = "1";
        });
    }

    function onTitleChanged() {
        article.title = document.getElementById("title-element").innerText;
    }

    function onTextHTMLChanged() {
        article.html = editor.innerHTML;
    }

    function getDraft() {
        refreshDraftData();
        return article;
    }

    function onDraftSelected(event) {
        fillEditorWithDraft(event.data);
    }

    function refreshDraftData() {
        article.title = document.getElementById("title-element").innerText;
        article.html = editor.innerHTML;
    }

    function onSaveButtonClicked() {
        ServerManager.saveDraft(getDraft())
        .then(function(savedDraft) {
            getDraft().id = savedDraft.id;
        });
    }

    function onDeleteButtonClicked() {
        ServerManager.deleteDraft(getDraft())
        .always(function() {
            fillEditorWithDraft(new Draft());
            ArticleList.toggle();
            ArticleList.toggle();
        });
    }

    function onPreviewButtonClicked() {
        PreviewManager.displayPreviewInElement(editor, "editor");
    }

    document.getElementById("file-input")
      .addEventListener("change", readFile, false);

    function readFile() {
        var file = event.target.files[0];
        var reader = new FileReader();
        reader.onload = function(event) {
            applyFormat("insertImage", event.target.result);
            onTextHTMLChanged();
        };
        reader.readAsDataURL(file);
    }

    <!-- Formatter -->


    function applyFormat(command, value, autoPassTreshhold) {
        if (!autoPassTreshhold && ((new Date().getTime() - latestFormatDate.getTime()) / 1000) < 0.1) { return; }
        latestFormatDate = new Date();
        document.execCommand(command, false, value);
        editor.focus();
        refreshDraftData();
    }

    function onBoldButtonClicked() {
        applyFormat("bold");
    }

    function onItalicButtonClicked() {
        applyFormat("italic");
    }

    function onUnderlineButtonClicked() {
        applyFormat("underline");
    }

    function onStrikeThroughButtonClicked() {
        applyFormat("strikethrough");
    }

    function onAlignCenterButtonClicked() {
        applyFormat("justifyCenter");
    }

    function onAlignJustifyButtonClicked() {
        applyFormat("justifyFull");
    }

    function onAlignLeftButtonClicked() {
        applyFormat("justifyLeft");
    }

    function onAlignRightButtonClicked() {
        applyFormat("justifyRight");
    }

    function onIndentIncreaseButtonClicked() {
        applyFormat("indent");
    }

    function onIndentDecreaseButtonClicked() {
        applyFormat("outdent");
    }

    function onTextLevelHeadingButtonClicked() {
        onTextLevelTextButtonClicked();
        applyFormat("fontSize","5", true);
        applyFormat("formatblock","blockquote", true);
    }

    function onTextLevelTextButtonClicked() {
        applyFormat("justifyLeft");
        applyFormat("fontSize", "4", true);
        applyFormat("foreColor", "#000", true);
        applyFormat("formatblock","p", true);
    }

    function onTextLevelQuoteButtonClicked() {
        onTextLevelTextButtonClicked();
        applyFormat("justifyRight", undefined, true);
        applyFormat("foreColor", "#f06868", true);
    }


    that.init = init;
    that.initWithDraft = initWithDraft;
    that.fillEditorWithDraft = fillEditorWithDraft;
    that.getDraft = getDraft;
    return that;
}());
