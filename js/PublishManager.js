var PublishManager = PublishManager || {};
PublishManager = (function () {
    "use strict";
    /* eslint-env browser  */

    var that = {};

    function preparePublishingDraft() {
        var currentDraft = EditorViewController.getDraft();
        sessionStorage.currentDraft = currentDraft.serialized();
    }

    function articleIsValid() {
        var draftDate = new Date(Draft.deserializedFrom(sessionStorage.currentDraft).date);
        if (Object.prototype.toString.call(draftDate) === "[object Date]") {
            if (isNaN(draftDate.getTime())) {
                return false;
            }
            else {
                return true;
            }
        }
        else {
            return false;
        }
    }

    function setArticleDate(date) {
        var currentDraft = Draft.deserializedFrom(sessionStorage.currentDraft);
        currentDraft.date = date;
        sessionStorage.currentDraft = currentDraft.serialized();
    }

    function publishArticle() {
        var currentDraft = Draft.deserializedFrom(sessionStorage.currentDraft);
        var article = ObjectConverter.parseArticleFromDraft(currentDraft);

        ServerManager.init();
        ServerManager.saveArticle(article)
        .then(function() {
            AppService.deleteCachedDraft();
            document.getElementById("publish-button").innerText = "CONGRATS! YOUR ARTICLE WAS PUBLISHED SUCCESSFULLY!";
        });
    }

    that.setArticleDate = setArticleDate;
    that.preparePublishingDraft = preparePublishingDraft;
    that.publishArticle = publishArticle;
    that.articleIsValid = articleIsValid;
    return that;
}());
