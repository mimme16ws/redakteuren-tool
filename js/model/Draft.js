var ObjectConverter = ObjectConverter || {};
"use strict";

class Draft extends Parse.Object {

    constructor(id, title, date, imageURL, html) {
        super("Draft");
        this.id = id ? id: "";
        this.title = title ? title : "";
        this.date = date ? date : new Date();
        this.imageURL = imageURL ? imageURL : undefined;
        this.html = html ? html : "";
    }

    set title(value) {
        super["title"] = value;
    }

    get title() {
        return super["title"];
    }

    set date(value) {
        super["date"] = value;
    }

    get date() {
        return super["date"];
    }

    set image(file) {
        super["image"] = file;
    }

    set html(value) {
        super["html"] = value;
    }

    get html() {
        return super["html"];
    }

    set isPublic(value) {
        super["isPublic"] = value;
    }

    get isPublic() {
        return super["isPublic"];
    }

    serialized() {
        var serializationObject = {
            id: this.id,
            title: this.title,
            imageURL: this.imageURL,
            image: undefined,
            html: this.html
        };
        return JSON.stringify(serializationObject);
    }

    static deserializedFrom(json) {
        var parsedJSON = JSON.parse(json);
        return new Draft(
            parsedJSON.id,
            parsedJSON.title,
            new Date(parsedJSON.date),
            parsedJSON.imageURL,
            parsedJSON.html
        );
    }
}

ObjectConverter = (function () {
    "use strict";
    /* eslint-env browser  */

    var that = {};

    function draftFromParseObject(object) {
        return new Draft(
            object.id,
            object.get("title"),
            undefined, // dates a not needed for drafts
            object.get("imageURL") ? object.get("imageURL") : "url(../assets/BehindSenseLogo.png)",
            object.get("html")
        );
    }

    function parseArticleFromDraft(draft) {
        var ParseArticle = Parse.Object.extend("Article");
        var article = new ParseArticle();
        article.set("title", draft.title);
        article.set("date", new Date());
        article.set("imageURL", draft.imageURL);
        article.set("html", draft.html);
        return article;
    }

    that.draftFromParseObject = draftFromParseObject;
    that.parseArticleFromDraft = parseArticleFromDraft;
    return that;
}());

Parse.Object.registerSubclass("Draft", Draft);
