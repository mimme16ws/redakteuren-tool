var ServerManager = ServerManager || {};
ServerManager = (function () {
    "use strict";
    /* eslint-env browser  */


    var that = {},
        latestDraftSaveDate = new Date();

    function init() {
        initParse();
    }

    function initParse() {
        Parse.initialize("5C4Ds1C8rbsVqfVOxik9y6QpnDSbpgXJ4ym0C0As", "6TuB8BZraxGKxknlLCDq799zo2BToJyNSTCCwh6f");
    }

    function saveArticle(article) {
        return article.save();
    }

    function fetchLatestDraft(user) {
        var DraftClass = Parse.Object.extend("Draft");
        var query = new Parse.Query(DraftClass);
        query.equalTo("authorId", user.id);
        query.descending("updatedAt");
        return query.first();
    }

    function fetchArticles() {
        var ArticleClass = Parse.Object.extend("Article");
        var query = new Parse.Query(ArticleClass);
        return query.find();
    }

    function fetchUserDrafts(user) {
        var DraftClass = Parse.Object.extend("Draft");
        var query = new Parse.Query(DraftClass);
        query.equalTo("authorId", user.id);
        query.descending("updatedAt");
        return query.find();
    }

    function fetchDraftWithId(id) {
        var ParseDraft = Parse.Object.extend("Draft");
        var query = new Parse.Query(ParseDraft);
        return query.get(id);
    }

    function deleteDraft(draftToDelete) {
        return fetchDraftWithId(draftToDelete.id)
        .always(function(draft) {
            return draft.destroy();
        });
    }

    function saveDraft(draftToSave) {
        if (((new Date().getTime() - latestDraftSaveDate.getTime()) / 1000) < 0.1) { return new Parse.Promise(); }
        latestDraftSaveDate = new Date();
        if (draftToSave.id) {
            return fetchDraftWithId(draftToSave.id)
            .then(function(object) {
                object.set("title", draftToSave.title);
                object.set("image", draftToSave.image);
                object.set("imageURL", draftToSave.imageURL);
                object.set("html", draftToSave.html);
                return object.save();
            },
            function() {
                return saveNewDraft(draftToSave);
            });
        } else {
            return saveNewDraft(draftToSave);
        }
    }

    function saveNewDraft(draftToSave) {
        var ParseDraft = Parse.Object.extend("Draft");
        var draft = new ParseDraft();
        draft.set("authorId", AppService.user().id);
        draft.set("image", draftToSave.image);
        draft.set("title", draftToSave.title);
        draft.set("html", draftToSave.html);
        return draft.save();
    }

    that.init = init;
    that.saveDraft = saveDraft;
    that.saveArticle = saveArticle;
    that.fetchLatestDraft = fetchLatestDraft;
    that.fetchArticles = fetchArticles;
    that.fetchUserDrafts = fetchUserDrafts;
    that.fetchDraftWithId = fetchDraftWithId;
    that.deleteDraft = deleteDraft;
    return that;
}());
