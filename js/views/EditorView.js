var EditorView = EditorView || {};
EditorView = (function () {
    "use strict";
    /* eslint-env browser  */

    var that = new EventPublisher(),
        titleElement,
        textEditor,
        img;

    function init() {
        addEventListeners();
        img = new Image();
        img.setAttribute("crossOrigin", "anonymous");
        onDropOver();
        onDrop();
    }

    function addEventListeners() {
        addTitleListener();
        addTextListener();
    }

    function addTitleListener() {
        titleElement = document.getElementById("title-position");
        titleElement.addEventListener("keypress", function() {
            var title = $("#workarea-title").text();
            that.notifyAll("titleDidChange", title);
        });
    }

    function addTextListener() {
        textEditor = document.getElementById("text-editor");
        textEditor.placeholder = "WRITE YOUR ARTICLE HERE...";
        textEditor.addEventListener("keypress", function() {
            var html = $("#text-editor").html();
            that.notifyAll("textDidChange", html);
        });
    }


    function onDropOver() {
        var pictureArea = document.getElementById("picture-area");
        pictureArea.addEventListener("dragover", function(e) {
            if (e.preventDefault) {
                e.preventDefault();
            }

            e.dataTransfer.dropEffect = "move";

            return false;
        });
    }

    function onDrop() {
        var pictureArea = document.getElementById("picture-area");
        var reader  = new FileReader();
        pictureArea.addEventListener("drop", function(event) {
            var file = event.dataTransfer.files[0];

            if (event.preventDefault) { event.preventDefault(); }
            if (event.stopPropagation) { event.stopPropagation(); }

            

            reader.addEventListener("load", function () {
                pictureArea.style.background = "url(" + reader.result + ") no-repeat center center";
                pictureArea.style.backgroundSize = "cover";
                that.notifyAll("titleImageURLDidChange", reader.result);
            }, false);

            if (file) {
                reader.readAsDataURL(file);
            }

            return false;
        });
    }

    that.init = init;
    return that;
}());
