var SideMenu = SideMenu || {};
SideMenu = (function () {
    "use strict";
    /* eslint-env browser  */

    var that = new EventPublisher();

    function init() {
        addEventListeners();
    }

    function addEventListeners() {
        addPublishButtonEventListener();
        addBoldButtonEventListener();
        addItalicButtonEventListener();
        addUnderlinedButtonEventListener();
        addStrikethroughButtonEventListener();
        addAlignCenterButtonEventListener();
        addAlignJustifyButtonEventListener();
        addAlignLeftButtonEventListener();
        addAlignRightButtonEventListener();
        addIndentIncreaseButtonEventListener();
        addIndentDecreaseButtonEventListener();
        addPreviewButtonListener();
        addTextLevelHeadingEventListener();
        addTextLevelTextEventListener();
        addTextLevelQuoteEventListener();
        addSaveButtonEventListener();
        addLogoutButtonEventListener();
        addDeleteButtonEventListener();
    }

    function addPublishButtonEventListener() {
        var publishButton = document.getElementById("publish-button");
        publishButton.addEventListener("click", function() {
            that.notifyAll("publishButtonClicked");
        });
    }

    function addDeleteButtonEventListener() {
        var button = document.getElementById("delete-button");
        button.addEventListener("click", function() {
            that.notifyAll("deleteButtonClicked");
        });
    }

    function addSaveButtonEventListener() {
        var button = document.getElementById("save-button");
        button.addEventListener("click", function() {
            that.notifyAll("saveButtonClicked");
        });
    }

    function addPreviewButtonListener() {
        var button = document.getElementById("preview-button");
        button.addEventListener("click", function() {
            that.notifyAll("previewButtonClicked");
        });
    }

    function addBoldButtonEventListener() {
        var boldButton = document.getElementById("bold-button");
        boldButton.addEventListener("click", function() {
            that.notifyAll("boldButtonClicked");
        });
    }

    function addItalicButtonEventListener() {
        var italicButton = document.getElementById("italic-button");
        italicButton.addEventListener("click", function() {
            that.notifyAll("italicButtonClicked");
        });
    }

    function addUnderlinedButtonEventListener() {
        var underlinedButton = document.getElementById("underlined-button");
        underlinedButton.addEventListener("click", function() {
            that.notifyAll("underlineButtonClicked");
        });
    }

    function addStrikethroughButtonEventListener() {
        var strikethroughButton = document.getElementById("strikethrough-button");
        strikethroughButton.addEventListener("click", function() {
            that.notifyAll("strikethroughButtonClicked");
        });
    }

    function addAlignCenterButtonEventListener() {
        var alignCenterButton = document.getElementById("alignCenter-button");
        alignCenterButton.addEventListener("click", function() {
            that.notifyAll("alignCenterButtonClicked");
        });
    }

    function addAlignJustifyButtonEventListener() {
        var alignJustifyButton = document.getElementById("alignJustify-button");
        alignJustifyButton.addEventListener("click", function() {
            that.notifyAll("alignJustifyButtonClicked");
        });
    }

    function addAlignLeftButtonEventListener() {
        var alignLeftButton = document.getElementById("alignLeft-button");
        alignLeftButton.addEventListener("click", function() {
            that.notifyAll("alignLeftButtonClicked");
        });
    }

    function addAlignRightButtonEventListener() {
        var alignRightButton = document.getElementById("alignRight-button");
        alignRightButton.addEventListener("click", function() {
            that.notifyAll("alignRightButtonClicked");
        });
    }

    function addIndentIncreaseButtonEventListener() {
        var button = document.getElementById("indent-increase-button");
        button.addEventListener("click", function() {
            that.notifyAll("indentIncreaseButtonClicked");
        });
    }

    function addIndentDecreaseButtonEventListener() {
        var button = document.getElementById("indent-decrease-button");
        button.addEventListener("click", function() {
            that.notifyAll("indentDecreaseButtonClicked");
        });
    }

    function addTextLevelHeadingEventListener() {
        var button = document.getElementById("text-level-heading");
        button.addEventListener("click", function() {
            that.notifyAll("textLevelHeadingButtonClicked");
        });
    }

    function addTextLevelTextEventListener() {
        var button = document.getElementById("text-level-text");
        button.addEventListener("click", function() {
            that.notifyAll("textLevelTextButtonClicked");
        });
    }
    function addTextLevelQuoteEventListener() {
        var button = document.getElementById("text-level-quote");
        button.addEventListener("click", function() {
            that.notifyAll("textLevelQuoteButtonClicked");
        });
    }

    function addLogoutButtonEventListener() {
        var button = document.getElementById("log-out-button");
        button.addEventListener("click", function() {
            that.notifyAll("logoutButtonClicked");
        });
    }

    that.init = init;
    return that;
}());
