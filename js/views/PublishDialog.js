var PublishDialog = PublishDialog || {};
PublishDialog = (function () {
    "use strict";
    /* eslint-env browser  */

    var that = {},
        hasSetDate = false,
        hasPublished = false;

    function init() {
        selectedDate();
    }

    function publishButtonClicked() {
        var hoverCSS = "#footer-container:hover { cursor: 'default'; background-color: #F06868; }";

        if (hasPublished || !hasSetDate) {
            console.log('publishing');
            return;
        }
        PublishManager.publishArticle();
        appendStyleSheetWithCSS(hoverCSS);
        hasPublished = true
    }

    function onDateSelected(date) {
        PublishManager.setArticleDate(new Date(date));
        hasSetDate = true;
        enablePublishButton();
    }

    function enablePublishButton() {
        var hoverCSS = "#footer-container:hover { cursor: pointer; background-color: #FF847F; }";
        var backgroundCSS = "#footer-container { background-color: #F06868; }";
        appendStyleSheetWithCSS(hoverCSS);
        appendStyleSheetWithCSS(backgroundCSS);
    }

    function appendStyleSheetWithCSS(css) {
        var style = document.createElement("style");

        if (style.styleSheet) {
            style.styleSheet.cssText = css;
        } else {
            style.appendChild(document.createTextNode(css));
        }

        document.getElementsByTagName("head")[0].appendChild(style);
    }


    that.init = init;
    that.onDateSelected = onDateSelected;
    that.publishButtonClicked = publishButtonClicked;
    return that;
}());
