var ArticleList = ArticleList || {};
ArticleList = (function () {
    "use strict";
    /* eslint-env browser  */

    var that = new EventPublisher(),
        dataSource = [],
        isVisible = false,
        element = document.getElementsByClassName("article-list")[0];

    function init() {
        reloadData();
    }

    function toggle() {
        element.style.display = isVisible ? "none" : "inline-block";
        isVisible = !isVisible;
        reloadData();
    }

    function reloadData() {
        ServerManager.init();
        ServerManager.fetchUserDrafts(AppService.user())
        .then(function(drafts) {
            dataSource = drafts;
            $(".article-list-item").remove();
            for (var i = 0; i < dataSource.length; i++) {
                elementForObjectAtRowIndex(dataSource[i], i);
            }
        });
    }


    function elementForObjectAtRowIndex(object, index) {
        var months = ["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"];
        var date = object.get("updatedAt");
        var node = document.createElement("div");
        node.className = "article-list-item";
        node.id = "item-index=" + index;
        if (object.get("imageURL")) { node.style.backgroundImage = "url(" + object.get("imageURL") + ")"; }
        node.innerHTML = "<div class='dim-view'></div><h2 class='article-list-item-title'>"
            + object.get("title").replace(/\\n/g, " ") + "</h2><h3 class='article-list-item-detail'>"
            + "updated on " + date.getDate() + ". " + months[date.getMonth()] + " " + date.getFullYear(); + "</h3>";
        node.addEventListener("click", function() {
            didSelectItemAtRowIndex(index);
        });
        document.getElementsByClassName("article-list")[0].insertBefore(node, document.getElementById("article-list-compose"));
    }

    function didSelectItemAtRowIndex(index) {
        var object = dataSource[index];
        var draft = ObjectConverter.draftFromParseObject(object);
        var previousDraft;
        try {
            if (EditorViewController) {
                that.notifyAll("didSelectItem", draft);
                EditorViewController.initWithDraft(draft);
                previousDraft = EditorViewController.getDraft();
                ServerManager.saveDraft(previousDraft)
                .then(function() {});
            }
        } catch(e) {
            that.notifyAll("didSelectItem", draft);
        }
    }


    that.init = init;
    that.toggle = toggle;
    return that;
}());
