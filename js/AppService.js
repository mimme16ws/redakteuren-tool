var AppService = AppService || {};
AppService = (function () {
    "use strict";
    /* eslint-env browser  */

    var that = {};

    function init() {
        var userCredentials = JSON.parse(sessionStorage.userCredentials);
        ServerManager.init();
        SideMenu.init();
        EditorViewController.init();

        addAppEventsListeners();
        login(userCredentials.username, userCredentials.password)
        .then(function() {
            ArticleList.init();
        });
        openEditor();
    }

    function openEditor() {
        if (sessionStorage.selectedDraft) {
            EditorViewController.initWithDraft(Draft.deserializedFrom(sessionStorage.selectedDraft));
        } else {
            EditorViewController.init();
        }
    }

    function showMainPage() {
        window.location = "index.html";
    }

    function startNewDraft() {
        ServerManager.saveDraft(EditorViewController.getDraft())
        .then(function() {
            deleteCachedDraft();
            EditorViewController.initWithDraft(new Draft());
        });
    }

    function deleteCachedDraft() {
        sessionStorage.removeItem("currentDraft");
        sessionStorage.removeItem("selectedDraft");
    }

    function addAppEventsListeners() {
        addPublishEventListener();
        SideMenu.addEventListener("logoutButtonClicked", logout);
    }

    function showLoadingSpinnerWithId(id) {
        document.getElementById("loader" + id).style.display = "block";
    }

    function hideLoadingSpinnerWithId(id) {
        document.getElementById("loader" + id).style.display = "none";
    }

    function addPublishEventListener() {
        SideMenu.addEventListener("publishButtonClicked", function() {
            var draft = EditorViewController.getDraft();
            PublishManager.preparePublishingDraft(draft);
        });
    }

    function user() {
        return Parse.User.current();
    }

    function logout() {
        Parse.User.logOut()
        .then(function() {
            window.location = "onboarding.html";
        });
    }

    function login(username, password) {
        return Parse.User.logOut()
        .then(function() {
            return Parse.User.logIn(username, password);
        });
    }

    that.login = login;
    that.logout = logout;
    that.init = init;
    that.user = user;
    that.showMainPage = showMainPage;
    that.startNewDraft = startNewDraft;
    that.deleteCachedDraft = deleteCachedDraft;
    that.showLoadingSpinnerWithId = showLoadingSpinnerWithId;
    that.hideLoadingSpinnerWithId = hideLoadingSpinnerWithId;
    return that;
}());
