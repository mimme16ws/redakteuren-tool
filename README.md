#BehindSense Publisher

This repository contains a composer tool for BehindSense publishers.

It let's you write and design your articles and push them to the BehindSense database
to be published in the curated articles section of the iOS App.

#Instructions

To launch the program open onboarding.html with Chrome.

#Login

To login for testing use the credentials "TestAccount" with password "test".

#Registration

Registration is not possible and most likely never will.
The published articles are pushed directly to the BehindSense backendand and will be visibile in the app.
As long as the iOS app is still in development, the above mentioned test user is provided.
To get an account and publish articles, you have to be selected by the BehindSense team either by havening a beautiful BehindSense portfolio or
by getting contacted by us separatly.